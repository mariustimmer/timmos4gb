CC=/home/timmer/git/gbdk-2020/build/gbdk/bin/lcc
MKDIR=/usr/bin/mkdir
RM=/usr/bin/rm
DIRECTORY_SOURCE=./src
DIRECTORY_BUILD=./build
DIRECTORY_INCLUDE=./include

build:	$(DIRECTORY_SOURCE)/tiles.c $(DIRECTORY_SOURCE)/malloc.c $(DIRECTORY_SOURCE)/map_desktop.c $(DIRECTORY_SOURCE)/main.c
	$(MKDIR) -p "$(DIRECTORY_BUILD)"
	$(CC) \
		-I"$(DIRECTORY_INCLUDE)" \
		-o "$(DIRECTORY_BUILD)/timmos.gb" \
		"$(DIRECTORY_SOURCE)/tiles.c" \
		"$(DIRECTORY_SOURCE)/map_desktop.c" \
		"$(DIRECTORY_SOURCE)/malloc.c" \
		"$(DIRECTORY_SOURCE)/main.c"

hoodandloyal:
	$(MKDIR) -p "$(DIRECTORY_BUILD)"
	$(CC) \
		-o $(DIRECTORY_BUILD)/hal.gb \
		-I"$(DIRECTORY_INCLUDE)" \
		"$(DIRECTORY_SOURCE)/hal.c" \
		"$(DIRECTORY_SOURCE)/hal_map.c" \
		"$(DIRECTORY_SOURCE)/hood_and_loyal.c"

clean:
	$(RM) -rf \
	    "$(DIRECTORY_BUILD)"