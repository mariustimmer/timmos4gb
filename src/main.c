#include <stdio.h>
#include <gb/console.h>
#include <gb/drawing.h>
#include <gb/gb.h>
#include <string.h>

#include <malloc.h>
#include <tiles.h>
#include <map_desktop.h>

// List of all tiles
#define TILE_EMPTY  0x00
#define TILE_COPYRIGHT  0x01
#define TILE_BORDER  0x02
#define TILE_FLASH  0x03
#define TILE_MAXIMIZE  0x04
#define TILE_APPLE  0x05
#define TILE_HEART  0x06
#define TILE_HAND  0x07
#define TILE_CONFIG  0x08
#define TILE_DOG  0x09
#define TILE_ICECREAM  0x0A
#define TILE_RAIN  0x0B
#define TILE_DINO  0x0C
#define TILE_ARROW  0x0D
#define TILE_MINUS  0x0E
#define TILE_PLUS  0x0F
#define TILE_MAN  0x10
#define TILE_DIRECTORY  0x11
#define TILE_BRICK_LEFT  0x12
#define TILE_BRICK_MIDDLE 0x13
#define TILE_BRICK_RIGHT 0x14
#define TILE_SKULL  0x15
#define TILE_COOL_DUCK  0x16
#define TILE_BRICK_UPSIDE 0x17
#define TILE_BRICK_PANEL 0x18

// Sprite ids
#define SPRITE_ID_COPYRIGHT 0x00

char *input(const char message[]);

/**
 * Loads the tile date into the tile memory.
 */
void init_tiles()
{
    SPRITES_8x8;
    set_sprite_data(0x00, 0x19, TILE_COLLECTION);
    // Prepare the background
    set_bkg_data(0x00, 0x19, TILE_COLLECTION);
    set_bkg_tiles(0x00, 0x00, MAP_DESKTOPWidth, MAP_DESKTOPHeight, MAP_DESKTOP);
    // Mapping of tiles to sprites:
    set_sprite_tile(SPRITE_ID_COPYRIGHT, TILE_COPYRIGHT);
    SHOW_SPRITES;
}

/**
 * Init the whole system.
 */
void init()
{
    init_tiles();
    color(BLACK, WHITE, M_DRAWING);
    cls();
    DISPLAY_ON;
}

void titlescreen()
{
    move_sprite(0, 0x1D, 0x4F);
    UINT8 color_direction = 1;
    UINT8 current_color = BLACK;
    color(BLACK, WHITE, M_DRAWING);
    gotogxy(7, 6);
    gprintf("TimmOS");
    color(BLACK, WHITE, M_DRAWING);
    gotogxy(4, 8);
    gprintf("2020 by Timmer");
    // Draw the "multi color" line
    for (UINT8 x_pos = 0x05; x_pos < 0x9B; x_pos++)
    {
        color(current_color, WHITE, M_DRAWING);
        line(x_pos - 1, 0x3B, x_pos + 1, 0x3D);
        if (color_direction == 0)
        {
            current_color++;
            if (current_color >= BLACK)
            {
                color_direction = 1;
            }
        }
        else
        {
            current_color--;
            if (current_color <= LTGREY)
            {
                color_direction = 0;
            }
        }
    }
    // Draw the box
    color(BLACK, WHITE, M_DRAWING);
    box(0x04, 0x28, 0x9B, 0x4B, M_NOFILL);
    waitpad(J_START);
    set_sprite_tile(SPRITE_ID_COPYRIGHT, TILE_EMPTY);
}

void draw_desktop()
{
    cls();
    mode(M_DRAWING | M_NO_SCROLL | M_NO_INTERP);
    //color(LTGREY, BLACK, M_DRAWING);
    //box(0, 0, SCREENWIDTH, 14, M_FILL);
    //HIDE_SPRITES;
    //SHOW_BKG;
    gotogxy(0, 2);
    char *username = input("Username");
    gprintf("Hello %s!", username);
}

char *input(const char message[])
{
    char *input_text = (char*) malloc(sizeof(char) * 20);
    color(DKGREY, WHITE, M_DRAWING);
    gprintf("%s: ", message);
    color(BLACK, WHITE, M_DRAWING);
    gets(input_text);
    return input_text;
}

void main()
{
    init();
    titlescreen();
    draw_desktop();
}
