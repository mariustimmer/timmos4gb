#include <stdio.h>
#include <gb/console.h>
#include <gb/drawing.h>
#include <gb/gb.h>

#include <hal.h>
#include <hal_map.h>

#define TILE_EST_E		0x00
#define TILE_EST_S		0x01
#define TILE_EST_T		0x02
#define TILE_EST_DOT	0x03
#define TILE_EST_TWO	0x04
#define TILE_EST_ZERO	0x05
#define TILE_EMPTY		0x06

#define TILE_BEGIN_H		7
#define TILE_BEGIN_AND		37
#define TILE_BEGIN_L		43
#define TILE_BEGIN_DRATH	71

void main()
{
	SPRITES_8x8;
	set_bkg_data(0x00, 126, HAL_TILES);
	set_bkg_tiles(0x00, 0x00, HAL_MAPWidth, HAL_MAPHeight, HAL_MAP);
	SHOW_BKG;
	DISPLAY_ON;
}
