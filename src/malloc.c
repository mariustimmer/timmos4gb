#include <gb/malloc.h>
#include <malloc.h>
#include <stdbool.h>

UBYTE malloc_current_offset = 0x00;

UBYTE _malloc_wipe = true;

void *malloc(UBYTE size)
{
    if (malloc_current_offset == 0) {
        malloc_current_offset = malloc_heap_start;
    }
    void *p = (void *) malloc_current_offset;
    if (_malloc_wipe == true) {
        /**
         * Wipe allocated memory.
         */
        for (UBYTE i = 0x00; i < size; i++) {
            p[i] = &0;
        }
    }
    malloc_current_offset += size;
    return p;
}